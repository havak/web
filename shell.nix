{
  pkgs ? import <nixpkgs> {},
}:
let
  gem_setup = ''
    mkdir -p .nix-gems
    export GEM_HOME=$PWD/.nix-gems
    export GEM_PATH=$GEM_HOME
    export PATH=$GEM_HOME/bin:$PATH
  '';
in pkgs.mkShell {
  name = "nuccdc-web";
  buildInputs = with pkgs; [
    ruby.devEnv
    bundler
  ];

  shellHook = ''
    ${gem_setup}
  '';
}
